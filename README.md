# sites-élèves

Ce dépot porte la publication de pages internet rédigées par des élèves, notamment en SNT

- Placer le travail de chaque élèves dans un dossier, en vous assurant qu'il a bien nommer un fichier "index.html"
- Placer tous ces dossiers dans un dossier portant le nom de la classe ou du groupe
- Déposer tous ces éléments dans le dépôt, puis faire une validation
- Aller dans Déploiement -> Pages pour obtenir l'adresse de publication


